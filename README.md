# Brain tumor detection API

Cette API a été crée à l'aide Django REST framework. Celle-ci reçoit en entrée une image cérébrale présentant ou non une tumeur et propose en sortie une prédicion au moyen de différents modèles de classification de type CNN comme :

* VGG19;
* Resnet50;
* Inception-ResnetV2.

